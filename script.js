let playing = false;
let count = 0;
let XO = false;
let startReset = document.getElementById("startreset");
let chance = document.getElementById("chance");
let winnerValue = document.getElementById("winnerValue");

startReset.addEventListener('click',startResetGame);
document.getElementById("box1").addEventListener('click',checkPattern);
document.getElementById("box2").addEventListener('click',checkPattern);
document.getElementById("box3").addEventListener('click',checkPattern);
document.getElementById("box4").addEventListener('click',checkPattern);
document.getElementById("box5").addEventListener('click',checkPattern);
document.getElementById("box6").addEventListener('click',checkPattern);
document.getElementById("box7").addEventListener('click',checkPattern);
document.getElementById("box8").addEventListener('click',checkPattern);
document.getElementById("box9").addEventListener('click',checkPattern);

function setText(id){
    if(!XO)
        document.getElementById(id).innerHTML = 'X';
    else
        document.getElementById(id).innerHTML = 'O';
}
function setText(id, text){
    document.getElementById(id).innerHTML = text;
}

function getText(str){
//    console.log(document.getElementById(str).textContent)
    return (document.getElementById(str).textContent);
}
//function changeColor(str){
//    document.getElementById(str).style.color = '#fff';
//    document.getElementById(str).style.background = '#A2F6DB';
//}

function show(id){
    document.getElementById(id).style.display = 'block';
}

function hide(id){
    document.getElementById(id).style.display = 'none';
}

function startResetGame(e){
    if(playing === true){
        //game is on and you want to reset
        setText("startreset","Start Game");
        
        count = 0;
        
        console.log("HELLO FROM startResetGame");
        hide("winner");
        hide("gameover");
        clearBoxes();
    }else{
        //game is off and you want to start a new game
        setText("startreset","Reset Game!");    
        hide("gameover");
        hide("winner");
        XO = false;
        chance.innerHTML = '1';
        show("turn");
        setTimeout(function(){
                hide("turn");
        },1000)
    }
    playing =! playing;
}

function checkPattern(){
    if(playing && (this.textContent === ''))
    {
        if(!XO)
            {
                this.textContent = 'X';
                chance.innerHTML = '2';
            }
        else
            {
                this.textContent = 'O';
                chance.innerHTML = '1';
            }
        XO =! XO;
        show("turn");
        setTimeout(function(){
            hide("turn");
        },1000);
        count++;
        if(count > 4){
            console.log(count);
            checkThePattern();
        }
    }
    else {
        if(!playing)
            alert("Please start the game!")
        if(!(this.textContent === ''))
            alert("This box has been already filled. Please select another box!")
    }
    
}

function clearBoxes(){
    hide("turn");
    for(let i = 1; i < 10 ; i++){
        setText("box" + i,'');
    }
}

function checkThePattern(){
    for(let i=1;i < 8;i += 3){
        if((getText("box"+i) === getText("box"+(i+1))) && (getText("box"+(i+1)) === getText("box"+(i+2))))
        {
           if(getText("box"+i) === 'X')
               winner(1,i,i+1,i+2);
            if(getText("box"+i) === 'O')
                winner(2,i,i+1,i+2);
        }
    }
    for(i=1; i<4;i++){
        if((getText("box"+i) == getText("box"+(i+3))) &&(getText("box"+(i+3)) == getText("box"+(i+6))))
        {
           if(getText("box"+i) === 'X')
               winner(1,i,i+3,i+6);
            if(getText("box"+i) === 'O')
                winner(2,i,i+3,i+6);
            
        }
    }
    if((getText("box1") == getText("box5")) &&(getText("box5") == getText("box9")))
    {
       if(getText("box1") === 'X')
           winner(1,1,5,9);
        if(getText("box1") === 'O')
            winner(2,1,5,9);
    }
    if((getText("box3") == getText("box5")) && (getText("box5") == getText("box7")))
    {
       if(getText("box3") === 'X')
           winner(1,3,5,9);
        if(getText("box3") === 'O')
            winner(2,3,5,9);
        
    }
    if(count == 9){
        setText("gameover","<p>Game Over!</p><p>It is a tie!</p>");
        show("gameover");
        playing = false;
        clearBoxes();
        setText("startreset","Start Game!");
    }        
}

function winner(num,i,j,k){
    count--;
    hide("turn");
    clearBoxes();
    winnerValue.innerHTML = num;
    show("winner");
    setTimeout(function(){
        hide("winner");
    },1000);
    count = 0;
    show("gameover");
    hide("winner");
    setTimeout(function(){
        hide("winner");
    },1000);
    setText("gameover","<p>Game Over!</p><p>Player "+ num +" wins </p>");
    playing = false;
//    console.log("HELLO from winner");
    setText("startreset","Start Game");
}

